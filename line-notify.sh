#!/bin/bash
# Set these variables
accessToken=$1
Message=$2
#PICTURE_PATH=$3

#curl -X POST -H "Authorization: Bearer $accessToken" -F "message=$Message" -F "imageFile=@$PICTURE_PATH" https://notify-api.line.me/api/notify
curl -X POST -H "Authorization: Bearer $accessToken" -F "message=$Message" https://notify-api.line.me/api/notify
